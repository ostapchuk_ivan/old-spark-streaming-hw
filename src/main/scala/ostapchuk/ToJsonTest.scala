package ostapchuk

import org.apache.spark.SparkConf
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}



object ToJsonTest {
    val conf = new SparkConf()
        //.set("spark.driver.memory", "4g")
        .set("spark.executor.extraJavaOptions", "-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:G1GC")

    val spark = SparkSession
        .builder
        .appName("StructuredNetworkWordCount")
        .master("local[20]")
        .config(conf)
        .getOrCreate()

    import spark.implicits._

    def main(args: Array[String]): Unit = {
        val df = getExpedia()

        val schema = df.schema

        println(schema.json)

//        val keyValue = hotels.selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
//
//        keyValue.show()
//
//        val fromJson = keyValue
//            .select(from_json(col("value").cast("string"),
//                schema).as("data"))
//            .select("data.*")

        //fromJson.show()
    }

    def getHotels() : DataFrame = {
        val hotels = spark.read
            .option("sep", ",")
            .option("header", "true")
            .csv("E:\\BigData\\SparkStreaming\\src\\main\\resources\\hotels")

        return hotels
    }

    def getWeather() : DataFrame = {
        val weather = spark.read
            .parquet("E:\\BigData\\SparkStreaming\\src\\main\\resources\\weather")
            .select("lng", "lat", "avg_tmpr_f", "avg_tmpr_c", "wthr_date")

        return weather
    }

    def getExpedia() : DataFrame = {
        val expedia = spark.read
            .format("avro")
            .load("E:\\BigData\\SparkStreaming\\src\\main\\resources\\expedia\\expedia")

        return expedia
    }
}


