package ostapchuk

import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.sql.avro._
import org.apache.avro.SchemaBuilder
import org.apache.spark.SparkConf
import org.apache.spark.sql.catalyst.expressions.DateDiff
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions.{datediff, expr, round, max, when, current_timestamp}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types._

object Playground {
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf()
            //.set("spark.driver.memory", "4g")
            .set("spark.executor.extraJavaOptions", "-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:G1GC")

        val spark = SparkSession
            .builder
            .appName("StructuredNetworkWordCount")
            .master("local[20]")
            .config(conf)
            .getOrCreate()

        import spark.implicits._

        val expedia16 = spark.read
            .format("avro")
            .load("E:\\BigData\\SparkStreaming\\src\\main\\resources\\moreExpedia\\part-00015-2d1e4278-ff68-4763-8185-da940ab48cde-c000.avro")
            .select(
                col("srch_ci"), col("srch_co"),
                col("hotel_id"))
            .withColumn("stay_duration16", datediff(col("srch_co"), col("srch_ci")))
            .withColumn("stay_type16",
                when(col("stay_duration16") >= 1 &&
                    col("stay_duration16") <= 3,
                    "Short stay")
                    .when(col("stay_duration16") >= 4 &&
                        col("stay_duration16") <= 7,
                        "Standart stay")
                    .when(col("stay_duration16") >= 8 &&
                        col("stay_duration16") <= 14,
                        "Standart extended stay")
                    .when(col("stay_duration16") >= 15 &&
                        col("stay_duration16") <= 30,
                        "Long stay")
                    .otherwise("Erroneous data"))
            .withColumnRenamed("srch_co", "srch_co16")
            .withColumnRenamed("hotel_id", "hotel_id16")
            .withColumnRenamed("srch_ci", "srch_ci16")
            .filter(col("hotel_id16") < 3)

        val filterCond = expedia16.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val filteredDf = expedia16.filter(filterCond)

        filteredDf.show()
        //expedia16.groupBy(asc("id"))

        val customerPreferences = filteredDf
            .groupBy("hotel_id16", "stay_type16")
            .count()
            .groupBy("hotel_id16")
            .agg(max( struct("count","stay_type16")).as("max"))
                .select($"hotel_id16",
                    $"max.count".as("max"),
                    $"max.stay_type16".as("customer_preference"))
                //.join(customerPreferences)

        customerPreferences.show()
        //            .withColumn("max_stay_type",
        //                max("count") over Window.partitionBy("hotel_id16"))
        //            .filter(col("max(count)") === col("max_stay_type"))
        //            .withColumnRenamed("stay_type16", "customer_preference")
        //            //.filter(col("customer_preference") =!= "Short stay").sort("id")
        //                .drop( "count")

        //        val lastOne = expedia16.join(customerPreferences,
        //            customerPreferences("hotel_id16") <=> expedia16("hotel_id16"))
        //
        //        lastOne.show(1000)


        spark.stop()
    }
}
