package ostapchuk

import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.SparkConf
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions.{col, datediff, expr, max, round, when, _}

/**
  * todo Perform stateful streaming with Spark Structured Streaming.
  *
  * done Read Expedia data for 2016 year from HDFS as initial state DataFrame.
  *
  * done Read data for 2017 year as streaming data.
  *
  * done Enrich both DataFrames with weather:
  * add average day temperature at checkin
  * (join with hotels+weather data from Kafka topic).
  *
  * done Filter incoming data by having average temperature more than 0 Celsius degrees.
  *
  * done Calculate customer's duration of stay as days
  * between requested check-in and check-out date.
  *
  * todo Create customer preferences of stay time based on next logic.
  *
  * todo Map each hotel with multi-dimensional state consisting of
  * record counts for each type of stay:
  * "Erroneous data": null, more than month(30 days), less than or equal to 0
  * "Short stay": 1-3 days
  * "Standart stay": 4-7 days
  * "Standart extended stay": 1-2 weeks
  * "Long stay": 2-4 weeks (less than month)
  * And resulting type for a hotel (with max count)
  *
  * done Apply additional variance with filter on children presence in booking (x2 possible states).
  *
  * todo Store final data in HDFS.
  */

class Model(args: Array[String]) {
    val staticExpediaPath: String = args(0)
    val checkPoint = args(1)
    val hdfsPath = args(2)

//    val conf = new SparkConf()
//        .set("spark.driver.memory", "3g")
//        .set("spark.executor.memory", "3g")
//        .set("spark.memory.offHeap.enabled", "true")
//        .set("spark.memory.offHeap.size", "10gb")
//        .set("spark.executor.extraJavaOptions", "-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:G1GC")

    val spark = SparkSession
        .builder
        .appName("StructuredNetworkWordCount")
        .master("local[2]")
//        .config(conf)
        .getOrCreate()

    import spark.implicits._

    val staticExpedia = spark.sparkContext.broadcast(getStaticExpedia())

    def start(): Unit = {
        if(args.length != 3) {
            println("Usage: <path int hdfs to static expedia> <checkpoint location> <path in hdfs to write results to>")
            return
        } else write(joinHotelsExpedia(), hdfsPath)
    }

    def write(toWrite: DataFrame, path: String): Unit = {
        val testConsole = toWrite
            .writeStream
            .format("csv")
            .outputMode("append")
            .option("path", "results")
            .option("checkpointLocation", "E:\\BigData\\SparkStreaming\\chkpoint")
            .option("sep", ",")
            .option("header", "true")
            .start()

        testConsole.awaitTermination(900000)
    }

    def writeKafka(toWrite: DataFrame): Unit = {
        val kafka = toWrite
            .selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
            .writeStream
            .format("kafka")
            .outputMode("append")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("topic", "results")
            .option("kafka.linger.ms", 10000)
            .option("kafka.request.timeout.ms", 700000)
            .option("checkpointLocation", "E:\\BigData\\SparkStreaming\\kafka-point")
            .start()

        kafka.awaitTermination(900000)
    }

    def joinHotelsExpedia(): DataFrame = {
        val hotelsWeather = getJoinedHotelsWeather()
            //.drop("time_stamp")

        val expedia16 = staticExpedia.value

        val expedia17 = getStreamExpedia()
            //.withWatermark("time_stamp", "2 hours")

        val firstJoin = hotelsWeather.join(expedia16,
            hotelsWeather("wthr_date") <=> expedia16("srch_ci16")
                && hotelsWeather("id") <=> expedia16("hotel_id16"),
            "leftOuter")
            .withWatermark("time_stampH", "2 hours")

        //        //.drop("hotel_id")
        //
        val secondJoin = firstJoin.join(expedia17,
            expr("""
                      wthr_date = srch_ci17 AND
                      id = hotel_id17 AND
                      time_stampH = time_stamp AND
                      time_stampH >= time_stamp AND
                      time_stampH <= time_stamp + interval 30 minutes
                """),

            //                    firstJoin("wthr_date") <=> expedia17("srch_ci17") &&
            ////                        firstJoin("id") <=> expedia17("hotel_id17") &&
            ////                        firstJoin("time_stampH") <=> expedia17("time_stamp"),
            "leftOuter")
            .withColumn("hotel_id", coalesce($"hotel_id16", $"hotel_id17"))
            .filter($"hotel_id".isNotNull)
            .withColumn("srch_co", coalesce($"srch_co16", $"srch_co17"))
            .withColumn("srch_ci", coalesce($"srch_ci16", $"srch_ci17"))
            .withColumn("srch_children_cnt", coalesce($"srch_children_cnt16", $"srch_children_cnt17"))
            .withColumn("children_present", coalesce($"children_present16", $"children_present17"))
            .withColumn("stay_duration", coalesce($"stay_duration16", $"stay_duration17"))
            .filter($"hotel_id".isNotNull)
            .select(col("hotel_id"), col("lng"), col("lat"),
                col("avg_tmpr_c"), col("srch_ci"), col("srch_co"),
                col("srch_children_cnt"), col("children_present"),
                col("stay_duration"))

        return secondJoin
    }

    def getJoinedHotelsWeather(): DataFrame = {
        val hotels = getStreamHotels()
            .withColumn("time_stampH", lit(current_timestamp()))
            .withWatermark("time_stampH", "2 hours")

        val weather = getStreamWeather()
            .withColumn("time_stamp", lit(current_timestamp()))
            .withWatermark("time_stamp", "2 hours")

        val hotelsWeather = hotels.join(weather,
            expr(
                """
                    lngH = lng AND
                    latH = lat AND
                    time_stampH = time_stamp AND
                    time_stampH >= time_stamp AND
                    time_stampH <= time_stamp + interval 30 minutes
                 """),


            //            latH = lat AND
            //            lngH = lng AND
            //            hotels("latH") <=> weather("lat")
            //                && hotels("lngH") <=> weather("lng")
            //                && hotels("time_stampH") <=> weather("time_stamp"),
            "leftOuter")

        //                    .drop("latH", "lngH", "avg_tmpr_f")

        //        val hotelsWeatherWithWatermark = hotelsWeather

        return hotelsWeather.drop("time_stamp")
    }

    def createCustomerPreferences(dataFrame: DataFrame): Unit = {
        //todo do first agg, write in kafka, read from it, do second agg, write in hdfs
        val customerPreferences = dataFrame
            .groupBy("hotel_id17", "stay_type17")
            .count()
            .groupBy("hotel_id17")
            .agg(max(struct("count", "stay_type17")).as("max17"))
            .select($"hotel_id17",
                $"max17.count".as("max17"),
                $"max17.stay_type17".as("customer_preference17"))
    }

    def readHotelsKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "hotels")
            .option("maxOffsetsPerTrigger", 40000)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaHotelSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .select("Id", "Latitude", "Longitude") //add Name

        return toWrite
    }

    def getStreamHotels(): DataFrame = {
        val hotels = readHotelsKafka()
            .withColumnRenamed("Id", "id")
            .withColumnRenamed("Name", "name")
            .withColumnRenamed("Latitude", "latH")
            .withColumnRenamed("Longitude", "lngH")
            .withColumn("latH", round($"latH", 2))
            .withColumn("lngH", round($"lngH", 2))

        return hotels
    }

    def readWeatherKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "weather")
            .option("maxOffsetsPerTrigger", 400000)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaWeatherSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .drop("avg_tmpr_f", "year", "month", "day")

        return toWrite
    }

    def getStreamWeather(): DataFrame = {
        val weather = readWeatherKafka()
            .select("lng", "lat", "avg_tmpr_c", "wthr_date")
            .filter(col("avg_tmpr_c") > 0)
            .withColumn("lat", round($"lat", 2))
            .withColumn("lng", round($"lng", 2))
        //.dropDuplicates("lat", "lng")

        return weather
    }

    def getStaticExpedia(): DataFrame = {
        val expedia16 = spark.read
            .format("avro")
            .load(staticExpediaPath)
            .select(
                col("srch_ci"), col("srch_co"),
                col("hotel_id"), col("srch_children_cnt"))
            .withColumn("children_present", when(col("srch_children_cnt") <=> 0, "no")
                .otherwise("yes"))
            .withColumn("stay_duration16", datediff(col("srch_co"), col("srch_ci")))
            .withColumn("stay_type16",
                when(col("stay_duration16") >= 1 &&
                    col("stay_duration16") <= 3,
                    "Short stay")
                    .when(col("stay_duration16") >= 4 &&
                        col("stay_duration16") <= 7,
                        "Standart stay")
                    .when(col("stay_duration16") >= 8 &&
                        col("stay_duration16") <= 14,
                        "Standart extended stay")
                    .when(col("stay_duration16") >= 15 &&
                        col("stay_duration16") <= 30,
                        "Long stay")
                    .otherwise("Erroneous data"))
            .withColumnRenamed("srch_co", "srch_co16")
            .withColumnRenamed("srch_children_cnt", "srch_children_cnt16")
            .withColumnRenamed("hotel_id", "hotel_id16")
            .withColumnRenamed("srch_ci", "srch_ci16")
            .withColumnRenamed("children_present", "children_present16")

        val filterCond16 = expedia16.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val expedia16ToJoin = expedia16
            .filter(filterCond16)
            .where($"srch_ci".contains("2016"))

        val customerPreferences16 = expedia16ToJoin
            .groupBy("hotel_id16", "stay_type16")
            .count()
            .withColumn("max_stay_type16",
                max("count") over Window.partitionBy("hotel_id16"))
            .filter(col("count") === col("max_stay_type16"))
            .withColumnRenamed("stay_type16", "customer_preference16")
            .drop("count")
            .withColumnRenamed("hotel_id16", "hotel_id")

        val filteredExpedia16 = expedia16ToJoin.join(customerPreferences16,
            customerPreferences16("hotel_id") <=> expedia16ToJoin("hotel_id16"))

        return filteredExpedia16
    }

    def readExpediaKafka(): DataFrame = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "expedia")
            .option("maxOffsetsPerTrigger", 200000)
            .option("startingOffsets", "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaExpediaSchema

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")
            .select(
                col("srch_ci"), col("srch_co"),
                col("hotel_id"), col("srch_children_cnt"))
            .withColumn("time_stamp", lit(current_timestamp()))

        //        val toWriteWithWatermark = toWrite.withWatermark("time_stamp", "2 hours")

        return toWrite
    }

    def getStreamExpedia(): DataFrame = {
        val expedia17 = readExpediaKafka()
            .withColumn("children_present", when(col("srch_children_cnt") <=> 0, "no")
                .otherwise("yes"))
            .withColumn("stay_duration17", datediff(col("srch_co"), col("srch_ci")))
            .withColumn("stay_type17",
                when(col("stay_duration17") >= 1 &&
                    col("stay_duration17") <= 3,
                    "Short stay")
                    .when(col("stay_duration17") >= 4 &&
                        col("stay_duration17") <= 7,
                        "Standart stay")
                    .when(col("stay_duration17") >= 8 &&
                        col("stay_duration17") <= 14,
                        "Standart extended stay")
                    .when(col("stay_duration17") >= 15 &&
                        col("stay_duration17") <= 30,
                        "Long stay")
                    .otherwise("Erroneous data"))
            .withColumnRenamed("srch_co", "srch_co17")
            .withColumnRenamed("srch_children_cnt", "srch_children_cnt17")
            .withColumnRenamed("hotel_id", "hotel_id17")
            .withColumnRenamed("srch_ci", "srch_ci17")
            .withColumnRenamed("children_present", "children_present17")


        val filterCond17 = expedia17.columns.map(x => col(x).isNotNull).reduce(_ && _)

        val expedia17ToJoin = expedia17
            .filter(filterCond17)
            .where($"srch_ci".contains("2017"))

        return expedia17ToJoin
    }
}
