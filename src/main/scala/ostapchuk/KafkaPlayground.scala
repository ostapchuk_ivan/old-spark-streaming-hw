package ostapchuk

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object KafkaPlayground {
    val hotelsPath: String = "E:\\BigData\\SparkStreaming\\src\\main\\resources\\hotels"
    val weatherPath = "E:\\BigData\\SparkStreaming\\src\\main\\resources\\weather"
    val expediaPath = "E:\\BigData\\SparkStreaming\\src\\main\\resources\\expedia\\expedia"

    val conf = new SparkConf()
        //.set("spark.driver.memory", "4g")
        .set("spark.executor.extraJavaOptions", "-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:G1GC")

    val spark = SparkSession
        .builder
        .appName("StructuredNetworkWordCount")
        .master("local[5]")
        .config(conf)
        .getOrCreate()

    import spark.implicits._

    def main(args: Array[String]): Unit = {
       readExpediaKafka()
    }

    def readExpediaKafka() : Unit = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "expedia")
            .option("maxOffsetsPerTrigger", 50)
            .option("startingOffsets", 	 "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaExpediaSchema
        //        val keyValue = hotels.selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
        //
        //        keyValue.show()

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")

        toConsole(toWrite)
    }

    def readWeatherKafka() : Unit = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "weather")
            .option("maxOffsetsPerTrigger", 50)
            .option("startingOffsets", 	 "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaExpediaSchema
        //        val keyValue = hotels.selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
        //
        //        keyValue.show()

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
            schema).as("data"))
            .select("data.*")


        toConsole(toWrite)
    }

    def readHotelsKafka() : Unit = {
        val hotels = spark
            .readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("subscribe", "hotels")
            .option("maxOffsetsPerTrigger", 200)
            .option("startingOffsets", 	 "earliest")
            .load()

        val keyValue = hotels.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
            .as[(String, String)].toDF()

        val schema = Utils.kafkaHotelSchema
//        val keyValue = hotels.selectExpr("CAST(id AS STRING) AS key", "to_json(struct(*)) AS value")
//
//        keyValue.show()

        val toWrite = keyValue.select(from_json(col("value").cast("string"),
                schema).as("data"))
            .select("data.*")


        toConsole(toWrite)
    }

    def toConsole(toWrite: DataFrame): Unit = {
        val testConsole = toWrite.writeStream
            .format("console")
            .outputMode("append")
            .start()

        testConsole.awaitTermination(900000)
    }

    def write(toWrite: DataFrame): Unit = {
        val write = toWrite.writeStream
            .format("csv")
            .outputMode("append")
            .option("path", "results")
            .option("checkpointLocation", "E:\\BigData\\SparkStreaming\\chkpoint")
            .option("sep", ",")
            .option("header", "true")
            .start()

        write.awaitTermination(900000)
    }
    def writeToKafka() : Unit = {
        val dataFrame = getStreamExpedia(expediaPath)

        val keyValue = dataFrame.selectExpr("CAST(hotel_id AS STRING) AS key", "to_json(struct(*)) AS value")

        val writing = keyValue
            .writeStream
            .format("kafka")
            .outputMode("append")
            .option("kafka.bootstrap.servers", "104.198.162.127:6667")
            .option("topic", "expedia")
            .option("kafka.linger.ms", 100000)
            .option("kafka.request.timeout.ms", 7000000)
            .option("checkpointLocation", "E:\\BigData\\SparkStreaming\\kafka-point")
            .start()

        writing.awaitTermination(10000000)
    }



    def getStreamHotels(path: String): DataFrame = {
        val hotels = spark.readStream
            .schema(Utils.kafkaHotelSchema)
            .option("maxFilesPerTrigger", "1")
            .option("sep", ",")
            .option("header", "true")
            .csv(path)

        return hotels
    }
    def getStreamExpedia(path: String): DataFrame = {
        val expedia17 = spark.readStream
            .schema(Utils.kafkaExpediaSchema)
            .option("maxFilesPerTrigger", "1")
            .format("avro")
            .load(path)


        return expedia17
    }
    def getStreamWeather(path: String): DataFrame = {
        val weather = spark.readStream
            .schema(Utils.kafkaWeatherSchema)
            .option("maxFilesPerTrigger", "1")
            .parquet(path)

        return weather
    }
}
