package ostapchuk

import java.io.File

import org.apache.spark.sql.{DataFrame, SparkSession, functions}
import org.apache.spark.sql.avro._
import org.apache.avro.SchemaBuilder
import org.apache.spark.SparkConf
import org.apache.spark.sql.catalyst.expressions.DateDiff
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.streaming.Trigger
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.functions.{datediff, col, expr, max, round, when, _}


/**
  * todo Perform stateful streaming with Spark Structured Streaming.
  *
  * done Read Expedia data for 2016 year from HDFS as initial state DataFrame.
  *
  * done Read data for 2017 year as streaming data.
  *
  * todo Enrich both DataFrames with weather:
  * add average day temperature at checkin
  * (join with hotels+weather data from Kafka topic).
  *
  * done Filter incoming data by having average temperature more than 0 Celsius degrees.
  *
  * todo Calculate customer's duration of stay as days
  * between requested check-in and check-out date.
  *
  * todo Create customer preferences of stay time based on next logic.
  *
  * todo Map each hotel with multi-dimensional state consisting of
  * record counts for each type of stay:
  * "Erroneous data": null, more than month(30 days), less than or equal to 0
  * "Short stay": 1-3 days
  * "Standart stay": 4-7 days
  * "Standart extended stay": 1-2 weeks
  * "Long stay": 2-4 weeks (less than month)
  * And resulting type for a hotel (with max count)
  *
  * todo Apply additional variance with filter
  * on children presence in booking (x2 possible states).
  *
  * todo Store final data in HDFS.
  */

object Main {
//    val conf = new SparkConf()
//        //.set("spark.driver.memory", "4g")
//        .set("spark.executor.extraJavaOptions", "-XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:G1GC")
//
//    val spark = SparkSession
//        .builder
//        .appName("StructuredNetworkWordCount")
//        .master("local[20]")
//        .config(conf)
//        .getOrCreate()
//
//    import spark.implicits._

    def main(args: Array[String]): Unit = {
        val model = new Model(args)
        model.start()
//        test()
    }

//    def getWeaher() : DataFrame = {
//        val weather = spark.read
//            .parquet("E:\\BigData\\SparkStreaming\\src\\main\\resources\\weather")
//            //.withColumn("year", substring(col("wthr_date"), 0, 4))
//            //.withColumn("month", substring(col("wthr_date"), 6, 2))
//            //.withColumn("day", substring(col("wthr_date"), 9, 2))
//            .select(col("lng"), col("lat"), col("avg_tmpr_c"), col("wthr_date"))
//            .withColumn("lat", round($"lat", 2))
//            .withColumn("lng", round($"lng", 2))
//            .filter(col("avg_tmpr_c") > 0)
//
//        weather.show()
//
//        return weather
//    }
//
//    def getHotels() : DataFrame = {
//        val hotels = spark.read
//            .option("sep", ",")
//            .option("header", "true")
//            .schema(Utils.hotelsSchema)
//            .csv("E:\\BigData\\SparkStreaming\\src\\main\\resources\\hotels")
//            .select("Id", "Name", "Latitude", "Longitude")
//            .withColumnRenamed("Id", "id")
//            .withColumnRenamed("Name", "name")
//            .withColumnRenamed("Latitude", "latH")
//            .withColumnRenamed("Longitude", "lngH")
//            .withColumn("latH", round($"latH", 2))
//            .withColumn("lngH", round($"lngH", 2))
//
//        hotels.show()
//
//        return hotels
//    }
//
//    def getJoinedHotelsWeather() : DataFrame = {
//        val hotels = getHotels()
//
//        val weather = getWeaher()
//
//        val hotelsWeather = hotels.join(weather,
//            hotels("latH") <=> weather("lat")
//                && hotels("lngH") <=> weather("lng"))
//        //                    .drop("latH", "lngH", "avg_tmpr_f")
//
//        hotelsWeather.show()
//
//        return hotelsWeather
//    }
//
//    def getExpedia(year: String) : DataFrame = {
//        val smallerYear = year.substring(2)
//
//        val expedia = spark.read
//            .format("avro")
//            .load("E:\\BigData\\SparkStreaming\\src\\main\\resources\\expedia-static")
//            .select(
//                col("srch_ci"), col("srch_co"),
//                col("hotel_id"), col("srch_children_cnt"))
//            .withColumn("children_present", when(col("srch_children_cnt") <=> 0, "no")
//                .otherwise("yes"))
//            .withColumn("stay_duration", datediff(col("srch_co"), col("srch_ci")))
//            .withColumn("stay_type"+smallerYear,
//                when(col("stay_duration") >= 1 &&
//                    col("stay_duration") <= 3,
//                    "Short stay")
//                    .when(col("stay_duration") >= 4 &&
//                        col("stay_duration") <= 7,
//                        "Standart stay")
//                    .when(col("stay_duration") >= 8 &&
//                        col("stay_duration") <= 14,
//                        "Standart extended stay")
//                    .when(col("stay_duration") >= 15 &&
//                        col("stay_duration") <= 30,
//                        "Long stay")
//                    .otherwise("Erroneous data"))
//            .withColumnRenamed("srch_co", "srch_co"+smallerYear)
//            .withColumnRenamed("srch_children_cnt", "srch_children_cnt"+smallerYear)
//            .withColumnRenamed("hotel_id", "hotel_id"+smallerYear)
//            .withColumnRenamed("srch_ci", "srch_ci"+smallerYear)
//            .withColumnRenamed("children_present", "children_present"+smallerYear)
//            .withColumnRenamed("stay_duration", "stay_duration"+smallerYear)
//
//        val filterCond = expedia.columns.map(x => col(x).isNotNull).reduce(_ && _)
//
//        val filteredExpedia = expedia
//            .filter(filterCond)
//            .where($"srch_ci".contains(year))
//
//        filteredExpedia.show()
//
//        return filteredExpedia
//    }
//
//    def joinHotelsExpedia() : DataFrame = {
//        val hotelsWeather = getJoinedHotelsWeather()
//
//        val expedia16 = getExpedia("2016")
//
//        val expedia17 = getExpedia("2017")
//
//        val firstJoin = hotelsWeather.join(expedia16,
//            hotelsWeather("wthr_date") <=> expedia16("srch_ci16")
//                && hotelsWeather("id") <=> expedia16("hotel_id16"),
//            "left")
//            //.drop("hotel_id")
//
//        firstJoin.show()
//
//        val secondJoin = firstJoin.join(expedia17,
//            firstJoin("wthr_date") <=> expedia17("srch_ci17") &&
//                firstJoin("id") <=> expedia17("hotel_id17"),
//            "left")
//            .withColumn("hotel_id", coalesce($"hotel_id16", $"hotel_id17"))
//            .filter($"hotel_id".isNotNull)
//            .withColumn("srch_co", coalesce($"srch_co16", $"srch_co17"))
//            .withColumn("srch_ci", coalesce($"srch_ci16", $"srch_ci17"))
//            .withColumn("srch_children_cnt", coalesce($"srch_children_cnt16", $"srch_children_cnt17"))
//            .withColumn("children_present", coalesce($"children_present16", $"children_present17"))
//            .withColumn("stay_duration", coalesce($"stay_duration16", $"stay_duration17"))
//            .filter($"hotel_id".isNotNull)
//            .select(col("hotel_id"), col("lng"), col("lat"),
//                col("avg_tmpr_c"), col("srch_ci"), col("srch_co"),
//                col("srch_children_cnt"), col("children_present"),
//                col("stay_duration"))
//
//
//        secondJoin.show()
//
//        return secondJoin
//    }
//
//    def test(): Unit = {
//        joinHotelsExpedia()
//        spark.stop()
//
//    }
}